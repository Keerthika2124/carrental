package com.rental.entity;

import java.time.LocalDate;

import jakarta.persistence.Entity;
import jakarta.persistence.OneToOne;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
@Table(name = "bookings")
public class Bookings {

	private Integer bookingId;
	private String carType;
	@OneToOne
	private Car car;
	@OneToOne
	private UserDetails user;
	private LocalDate bookingDate;
	private String bookingStatus;
	private String rejectReason;

}
