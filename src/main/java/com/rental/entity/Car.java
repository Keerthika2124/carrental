package com.rental.entity;

import jakarta.persistence.Entity;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
@Table(name = "cars")
public class Car {

	private Integer carId;
	private String carName;
	private Integer quantity;
	private String carType;
	private Double carPrice;

}
